# 3018 Pro electronics case
Electronics enclosure for the GRBL board. No additional screws or hardware needed. Fully encloses the electronics to protect from debris. Also supports a 40mm fan for cooling the stepper drivers.

