## 3018 Pro emergency button
Emergency stop button that resets the mainboard when pressed. Based around an old computer power button (model Solteam PS3-22SP) with a satisfying click.
![Emergency button placement](button.png)

Font is comic sans because why not.
